using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class Node : MonoBehaviour
{
    public Node nodePadre;
    public float distancia;
    public int[] posicion;
    public bool comprobado;
 
    public string Info()
    {
        return $"Padre:{nodePadre}, Distancia:{distancia}, Posicion:{posicion[0]}-{posicion[1]}";
    }
}
