using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public static class NodeCreate
{
    public static List<Node> nodes = new();
    private static bool end = false;
    public static void FindSelectableNode()
    {
        Queue<Node> proses = new();
        proses.Enqueue(nodes[0]);
        nodes[0].comprobado = true;
        while (proses.Count > 0)
        {
            Queue<Node> oreden = Order(proses);
            proses.Clear();
            proses = oreden;
            Node nodeAct = proses.Dequeue();
            Queue<Node> list = FindAdjentNode(nodeAct);

            foreach (Node t in list)
            {
                if (!t.comprobado)
                {
                    t.nodePadre = nodeAct;
                    t.comprobado = true;
                    proses.Enqueue(t);
                    if (t.distancia == 0)
                    {
                        Win(t);
                        end = true;
                        break;
                    }
                }
            }
            if (end) break;
        }
    }
    public static Queue<Node> FindAdjentNode(Node n)
    {
        Queue<Node> nodesLocal = new();

        int i = nodes.Count - 1;
        int sum = 1;
        if (n.posicion[0] + 1 < Calculator.length)
        {
            int[] pos = new int[2];
            pos[0] = n.posicion[0] + 1;
            pos[1] = n.posicion[1];
            if (!CheckIfPosExist(pos))
            {
                GameManager.Instance.InstantiateToken(GameManager.Instance.token3, pos);
                nodesLocal.Enqueue(nodes[i + sum]);
                sum++;
            }
        }
        if (n.posicion[0] - 1 >= 0)
        {
            int[] pos = new int[2];
            pos[0] = n.posicion[0] - 1;
            pos[1] = n.posicion[1];
            if (!CheckIfPosExist(pos))
            {
                GameManager.Instance.InstantiateToken(GameManager.Instance.token3, pos);
                nodesLocal.Enqueue(nodes[i + sum]);
                sum++;
            }
        }
        if (n.posicion[1] + 1 < Calculator.length)
        {
            int[] pos = new int[2];
            pos[0] = n.posicion[0];
            pos[1] = n.posicion[1] + 1;
            if (!CheckIfPosExist(pos))
            {
                GameManager.Instance.InstantiateToken(GameManager.Instance.token3, pos);
                nodesLocal.Enqueue(nodes[i + sum]);
                sum++;
            }
        }
        if (n.posicion[1] - 1 >= 0)
        {
            int[] pos = new int[2];
            pos[0] = n.posicion[0];
            pos[1] = n.posicion[1] - 1;
            if (!CheckIfPosExist(pos))
            {
                GameManager.Instance.InstantiateToken(GameManager.Instance.token3, pos);
                nodesLocal.Enqueue(nodes[i + sum]);

            }
        }
        Queue<Node> nodesLocalOrden = Order(nodesLocal);
        return nodesLocalOrden;
    }
    public static bool CheckIfPosExist(int[] pos)
    {
        bool exist = false;
        foreach (Node n in nodes)
        {
            if (n.posicion[0] == pos[0] && n.posicion[1] == pos[1]) exist = true;
        }
        return exist;
    }
    public static Queue<Node> Order(Queue<Node> proses)
    {
        IEnumerable<Node> query = proses.OrderBy(node => node.distancia);
        Queue<Node> prosesOrdn = new();
        foreach (Node node in query)
        {
            prosesOrdn.Enqueue(node);
        }

        return prosesOrdn;

    }
    public static void Win(Node node)
    {
        Debug.Log(node.Info());
        GameManager.Instance.InstantiateToken(GameManager.Instance.token4, node.posicion);
        if (node.nodePadre != null) Win(node.nodePadre);
    }
}
