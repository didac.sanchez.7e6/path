using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public GameObject token1, token2, token3, token4;
    private int[,] GameMatrix; //0 not chosen, 1 player, 2 enemy
    public int[] startPos = new int[2];
    public int[] objectivePos = new int[2];
    private void Awake()
    {
        Instance = this;
        GameMatrix = new int[Calculator.length, Calculator.length];

        for (int i = 0; i < Calculator.length; i++) //fila
            for (int j = 0; j < Calculator.length; j++) //columna
                GameMatrix[i, j] = 0;

        //randomitzar pos final i inicial;
        var rand1 = Random.Range(0, Calculator.length);
        var rand2 = Random.Range(0, Calculator.length);
        startPos[0] = rand1;
        startPos[1] = rand2;
        SetObjectivePoint(startPos);

        GameMatrix[startPos[0], startPos[1]] = 1;
        GameMatrix[objectivePos[0], objectivePos[1]] = 2;

        InstantiateToken(token1, startPos);
        InstantiateToken(token2, objectivePos);
        InstantiateToken(token3,startPos);
        NodeCreate.FindSelectableNode();
        ShowMatrix();
    }
    public void InstantiateToken(GameObject token, int[] position)
    {
        var obj = Instantiate(token, Calculator.GetPositionFromMatrix(position),
            Quaternion.identity);
        if (obj.GetComponent<Node>() != null)
        {
            
            Node node = obj.GetComponent<Node>();
            node.posicion = position;
            node.distancia = Calculator.CheckDistanceToObj(position, objectivePos);
            node.comprobado = false;
            NodeCreate.nodes.Add(node);
        }
    }
    private void SetObjectivePoint(int[] startPos) 
    {
        var rand1 = Random.Range(0, Calculator.length);
        var rand2 = Random.Range(0, Calculator.length);
        if (rand1 != startPos[0] || rand2 != startPos[1])
        {
            objectivePos[0] = rand1;
            objectivePos[1] = rand2;
        }
    }

    private void ShowMatrix() //fa un debug log de la matriu
    {
        string matrix = "";
        for (int i = 0; i < Calculator.length; i++)
        {
            for (int j = 0; j < Calculator.length; j++)
            {
                matrix += GameMatrix[i, j] + " ";
            }
            matrix += "\n";
        }
        Debug.Log(matrix);
    }
    //EL VOSTRE EXERCICI COMEN�A AQUI
    private void Update()
    {
        if (!EvaluateWin())
        {

        }
    }
    private bool EvaluateWin()
    {
        return false;
    }
}
